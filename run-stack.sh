#!/bin/bash
#
# Monolith IDE Stack Builder
#
# (c) dogsbark Inc. - www.dogsbark.net
#

### Constants
export USER='loouis'
export TERM='xterm-256color'
export HOME="/home/${USER}/Documents/scripts/echoes"
export SYSTEM="/usr/local/bin"
WORKSPACE="${HOME}/workspace/"
SSH_DIR="${HOME}/.ssh/"
MONOLITH_DIR="${HOME}"
SERVER_SCRIPT="${MONOLITH_DIR}/bin/monolith.js"
PYTHON_ACTIVATE="/opt/virtualenv/bin/activate"
# Stack connections (0.0.0.0 is any IP)
HOSTNAME="0.0.0.0"
PORT="8787"
# Project title (without space)
PROJECTNAME="MyProject"
# Use commas for multiple user accounts M_USERS (without space)
EMAIL1="loouis@goodian.space"
PASS1="share"
EMAIL2="codereview@goodian.space"
PASS2="goody"
M_USERS="${EMAIL1}:${PASS1},${EMAIL2}:${PASS2}"
# Extra flags
CBFLAGS=""

### Variables provided by environment
# RSA_PRIVATE, RSA_PUBLIC
# EMAIL, NAME, USERNAME
# GIT_URL, GIT_USER, GIT_PASSWD (some private token)
# GIT_HOST, WEBHOOK_URL

function check_node () {
   # Check node installed
   if which node >/dev/null;
      then
         echo "[echoes] Node installed!"
      else
         # Load Node package
         echo "[echoes] Installing Node..."
         sudo bash bin/install-node.sh
   fi
}

function check_nv () {
   echo "[echoes] Swap node to v0.10.33..."
   
   # Swap/Install v0.10.33
   sudo n 0.10.33
}

function update_link () {

   file1="${SYSTEM}/echoes-start"
   file2="${SYSTEM}/echoes-purge"

   # Create symlink
   if [ -f "$file1" ]
      then
         echo "[echoes] Symlink <echoes-start> OK!"
      else
         echo "[echoes] Create <echoes-start> symlink..."
         
         sudo ln -s ${HOME}/run-stack.sh ${SYSTEM}/
         sudo mv ${SYSTEM}/run-stack.sh ${SYSTEM}/echoes-start
   fi
   
   if [ -f "$file2" ]
      then
         echo "[echoes] Symlink <echoes-purge> OK!"
      else
         echo "[echoes] Create <echoes-purge> symlink..."
         
         sudo ln -s ${HOME}/clean-stack.sh ${SYSTEM}/
         sudo mv ${SYSTEM}/clean-stack.sh ${SYSTEM}/echoes-purge
   fi
}

function setup_workspace () {
    echo "[echoes] Calling setup_workspace ..."

    # Create workspace dir
    mkdir -p ${WORKSPACE}
    mkdir -p ${HOME}/settings
}

function setup_ssh () {
    echo "[echoes] Calling setup_ssh ..."

    if [ ! "$RSA_PUBLIC" ] || [ ! "$RSA_PRIVATE" ]; then
        echo "[echoes] Skipping setup_ssh, no private and public keys to setup ..."
    fi

    # Ensure directory
    mkdir -p ${SSH_DIR}

    # Store/Update keys
    echo "${RSA_PUBLIC}" | tee "${SSH_DIR}id_rsa.pub"
    echo "${RSA_PRIVATE}" | tee "${SSH_DIR}id_rsa"

    chmod 600 "${SSH_DIR}id_rsa.pub"
    chmod 600 "${SSH_DIR}id_rsa"
}

function setup_netrc () {
    echo "[echoes] Calling setup_netrc ..."

    # No valid things to setup
    if [ ! $GIT_HOST ] || [ ! $GIT_USER ] || [ ! $GIT_PASSWD ]; then
        echo "[echoes] Skipping setup_netrc ..."
        return
    fi

    local filename="${HOME}/.netrc"

    # Exit if already there
    if grep -i "machine ${GIT_HOST}" $filename; then
        return
    fi

    # Git auth over http/https with token
    echo "machine ${GIT_HOST}
          login ${GIT_USER}
          password ${GIT_PASSWD}
         " >> $filename

    chmod 600 $filename
}

function setup_git () {
    echo "[echoes] Calling setup_git ..."

    # Skip if git directory exists
    if [ -d "$WORKSPACE.git" ]; then
        "Skipping setup_git because WORKSPACE is already setup ..."
        return
    fi

    if [ ! "$GIT_URL" ]; then
        echo "[echoes] Skipping setup_git because no GIT_URL given ..."
        echo "[echoes] Init empty git repository in workspace ..."
        git init ${WORKSPACE}
        return
    fi

    # Do cloning
    git clone ${GIT_URL} ${WORKSPACE}
}

function setup_hg () {
    echo "[echoes] Calling setup_hg ..."

    # Skip if git directory exists
    if [ -d "$WORKSPACE.hg" ]; then
        "[echoes] Skipping setup_hg because WORKSPACE is already setup ..."
        return
    fi

    if [ ! "$HG_URL" ]; then
        echo "[echoes] Skipping setup_hg because no HG_URL given ..."
        echo "[echoes] Init empty hg repository in workspace ..."
        hg init ${WORKSPACE}
        return
    fi

    # Do cloning
    hg clone ${HG_URL} ${WORKSPACE}
}

# Sets up a monolith sample if one exists
function setup_sample () {
    CBFLAGS="${CBFLAGS} --sample ${MONOLITHIO_STACK}"
}

# Sets up git or mercurial
function setup_repo () {
    echo "[echoes] Calling setup_repo ..."

    # Check if workspace directory already contains stuff
    if [ -n "$(ls -A ${WORKSPACE})" ]; then
        echo "[echoes] Skipping setup_repo because workspace folder is not empty"
        return
    fi

    # Check if we should setup either
    # git or mercurial based on env variables provided
    if [ -n "$GIT_URL" ]; then
        setup_git
        return
    elif [ -n "$HG_URL" ];then
        setup_hg
        return
    elif [ -n "$MONOLITHIO_STACK" ]; then
        setup_sample
    fi
}

function setup_perm () {
    echo "[echoes] Calling setup_perm ..."

    chown ${USER} -R ${HOME}
    chmod +x ${SSH_DIR}
    chmod 600 ${SSH_DIR}*

    # Ensure /tmp's permissions
    sudo chmod 777 /tmp
}

function setup_appengine () {
    # PHP and Python
    if [ -d "/opt/google_appengine" ]; then
        export PATH="/opt/google_appengine:${PATH}"
    # GO
    elif [ -d "/opt/go_appengine" ]; then
        export PATH="/opt/go_appengine:${PATH}"
        export GOROOT="/opt/go_appengine/goroot"
        export GOPATH="/opt/go_appengine/gopath"
    # Java
    elif [ -d "/opt/java_appengine" ]; then
        export PATH="/opt/java_appengine/bin:${PATH}"
    fi
}

function setup_env () {
    echo "[echoes] Calling setup_env ..."

    # Set home
    export M_USER=${USER}
    export WORKSPACE_DIR=${WORKSPACE}
    export WORKSPACE_ADDONS_DIR="${HOME}/.monolith-addons/"

    # Set command prompt
    export PS1="\[$(tput setaf 1)\]\u\[$(tput setaf 3)\] \W \[$(tput setaf 2)\]# \[$(tput sgr0)\]"

    # Set App Engine related variables
    setup_appengine

    # Unset sensitive stuff
    unset RSA_PRIVATE
    unset RSA_PUBLIC
    unset GIT_PASSWD
}

function setup_python () {
    echo "[echoes] Callling setup_python ..."
    
    if [ -f "${PYTHON_ACTIVATE}" ]; then
        source "${PYTHON_ACTIVATE}"
        return
    fi;
    
    echo "[echoes] Skipped setup_python ..."
}

function start_server () {
    echo "[echoes] Calling start_server ..."

    cd ${WORKSPACE}
    exec ${SERVER_SCRIPT} --hostname ${HOSTNAME} --port ${PORT} --users ${M_USERS} --title ${PROJECTNAME} run ${WORKSPACE} ${CBFLAGS}
}

### Initialize
#---------
check_node
check_nv
#---------
update_link
#---------
setup_workspace
setup_ssh
setup_netrc
setup_perm
setup_repo
setup_workspace
setup_env
setup_python
start_server

