#!/bin/bash

#
# Delete User Stack Data
#

### Constants
export USER="loouis"
export HOME="/home/${USER}/Documents/scripts/echoes/"
DIR_1="${HOME}/workspace"
DIR_2="${HOME}/.ssh"
DIR_3="${HOME}/.monolith-addons"
DIR_4="${HOME}/settings"
DIR_5="${HOME}/.config"
FILE_1="${HOME}/.bash_history"

function delete_data () {
   echo "[echoes] Deleting user stack data..."
   rm -rfv ${DIR_1} ${DIR_2} ${DIR_3} ${DIR_4} ${DIR_5}
   rm -rfv ${FILE_1}
   echo "[echoes] Cleaned up!"
}

function swap_n_back () {
   echo "[echoes] Swapping recommended node version back..."
   n latest
}

### Initialize
delete_data
swap_n_back
